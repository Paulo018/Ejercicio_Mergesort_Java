package Modelo;

public class Platillos 
{
    String Nombre;
    double Precio;
    int Codigo;

    public Platillos(String Nombre, double Precio, int Codigo) {
        this.Nombre = Nombre;
        this.Precio = Precio;
        this.Codigo = Codigo;
    }

    public Platillos(int Codigo) {
        this.Codigo = Codigo;
    }

    
    public Platillos(Object[] Registro) 
    {
        Nombre = Registro[0].toString();
        Precio = Double.parseDouble(Registro[1].toString());
        Codigo = Integer.parseInt(Registro[2].toString());
        
    } 
    
    public Object[] getRegistro()
    {
        return new Object[]{Nombre, Precio, Codigo};
    }
    
    public int[] getRegistrodecodigos()
    {
        return new int[]{Codigo};
    }
            
    public Platillos() {
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }
    
    
}