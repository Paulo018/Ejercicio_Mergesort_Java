package Control;


import Modelo.Platillos;
import javax.swing.table.DefaultTableModel;

public class Arreglo_Platillos 
{
    public Platillos[] Arreglo = new Platillos[30];
    int i=0;
    
    public void Agregar_Platillos(Platillos Elemento)
    {
        if(1<Arreglo.length)
        {
            Arreglo[i] = Elemento;
            i++;
        }
        
    }
    
    public void Listar(DefaultTableModel modTabla)
    {
        modTabla.setRowCount(0);
        for (int Pos = 0; Pos < i; Pos++) 
        {
            modTabla.addRow(Arreglo[Pos].getRegistro());
            
        }
    }
    
    public void Buscar(DefaultTableModel ModTabla, String Dato)
    {
        ModTabla.setRowCount(0);
        for (int Pos = 0; Pos < i; Pos++) 
        {
            if (Arreglo[Pos].getNombre().contains(Dato))
            {
                ModTabla.addRow(Arreglo[Pos].getRegistro());
            }
            
        }
    }
    
    public void Ordenar(Arreglo_Platillos Datos, int Inicio, int Fin)
    {
        if(Inicio<Fin)
        {
            int Medio = (Inicio+Fin)/2;  
            
            Ordenar(Datos, Inicio, Medio);    
            Ordenar(Datos, Medio +1,Fin );    
            
            merge(Datos, Inicio, Medio, Fin);    
        }
    }
    
    public void merge(Arreglo_Platillos Datos, int Inicio, int medio, int Final) {
        int n1 = medio - Inicio + 1;         
        int n2 = Final - medio;              
        Platillos[] Izquierda = new Platillos[n1 + 1];
        Platillos[] Derecha = new Platillos[n2 + 1];  
        for (int i = 0; i < n1; i++) {
            Izquierda[i] = Arreglo[Inicio + i];

        }
        for (int j = 0; j < n2; j++) 
        {
            Derecha[j] = Arreglo[medio + j + 1];
        }
        Izquierda[n1] = new Platillos(Integer.MAX_VALUE);
        Derecha[n2] = new Platillos(Integer.MAX_VALUE);
        int i = 0;
        int j = 0;
        for (int k = Inicio; k <= Final; k++) 
        {
            if (Izquierda[i].getCodigo() < Derecha[j].getCodigo()) 
            {
                Arreglo[k] = Izquierda[i]; ;
                i++;
            } 
            else
            {
                Arreglo[k] = Derecha[j]; 
                j++;
            }
        }
    }
    
   public Platillos getDatos(int Pos)
    {
        return Arreglo[Pos];
    }

    public int getI() {
        return i;
    }
}