
package Vista;

import Control.Arreglo_Platillos;
import Modelo.Platillos;
import java.awt.Color;
import java.awt.Font;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.KeyEvent;
import javafx.beans.binding.Bindings;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class JFrame_Principal extends javax.swing.JFrame 
{
    Arreglo_Platillos Lista_Plat = new Arreglo_Platillos();
    DefaultTableModel modTabla;
    DefaultTableModel modTabla1;

    
    public JFrame_Principal() {
        setUndecorated(true);
        initComponents();
        modTabla= (DefaultTableModel) tbl_Datos.getModel();
        setLocationRelativeTo(null);
        setResizable(false);
        tbl_Datos.getTableHeader().setFont(new Font("Verdana", 1, 14)); 
        tbl_Datos.getTableHeader().setBackground(Color.CYAN);
        tbl_Datos.getTableHeader().setEnabled(false);
        Color color = new Color(161, 136, 127, 100);
        Color color1 = new Color(161, 136, 127);
        tbl_Datos.getParent().setBackground(color);
        tbl_Datos.setBackground(color1);
        
        
        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        btn_cerrar = new javax.swing.JLabel();
        txt_Nombre = new javax.swing.JTextField();
        txt_Codigo = new javax.swing.JTextField();
        txt_Precio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_Datos = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        btn_Registrar = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btn_Listar = new javax.swing.JLabel();
        btn_Ordenar = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 300));

        btn_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_3-01.png"))); // NOI18N
        btn_cerrar.setText("jLabel3");
        btn_cerrar.setToolTipText("Cerrar");
        btn_cerrar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_cerrarMouseMoved(evt);
            }
        });
        btn_cerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_cerrarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_cerrarMouseExited(evt);
            }
        });
        jLayeredPane1.add(btn_cerrar);
        btn_cerrar.setBounds(470, 20, 100, 35);

        txt_Nombre.setMaximumSize(new java.awt.Dimension(12, 12));
        txt_Nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_NombreActionPerformed(evt);
            }
        });
        txt_Nombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_NombreKeyTyped(evt);
            }
        });
        jLayeredPane1.add(txt_Nombre);
        txt_Nombre.setBounds(100, 50, 190, 30);

        txt_Codigo.setMaximumSize(new java.awt.Dimension(12, 12));
        txt_Codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_CodigoActionPerformed(evt);
            }
        });
        txt_Codigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_CodigoKeyTyped(evt);
            }
        });
        jLayeredPane1.add(txt_Codigo);
        txt_Codigo.setBounds(100, 220, 190, 30);

        txt_Precio.setMaximumSize(new java.awt.Dimension(12, 12));
        txt_Precio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_PrecioActionPerformed(evt);
            }
        });
        txt_Precio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_PrecioKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_PrecioKeyTyped(evt);
            }
        });
        jLayeredPane1.add(txt_Precio);
        txt_Precio.setBounds(100, 130, 190, 30);

        tbl_Datos.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tbl_Datos.setForeground(new java.awt.Color(255, 255, 255));
        tbl_Datos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Platillo", "Precio", "Código"
            }
        ));
        tbl_Datos.setCellSelectionEnabled(true);
        tbl_Datos.setDoubleBuffered(true);
        tbl_Datos.setDragEnabled(true);
        tbl_Datos.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jScrollPane1.setViewportView(tbl_Datos);

        jLayeredPane1.add(jScrollPane1);
        jScrollPane1.setBounds(310, 120, 280, 160);

        jLabel9.setBackground(new java.awt.Color(153, 153, 255));
        jLabel9.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Código:");
        jLayeredPane1.add(jLabel9);
        jLabel9.setBounds(20, 190, 90, 25);

        btn_Registrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_4.png"))); // NOI18N
        btn_Registrar.setText("jLabel3");
        btn_Registrar.setToolTipText("Registrar");
        btn_Registrar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_RegistrarMouseMoved(evt);
            }
        });
        btn_Registrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_RegistrarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_RegistrarMouseExited(evt);
            }
        });
        jLayeredPane1.add(btn_Registrar);
        btn_Registrar.setBounds(330, 20, 100, 35);

        jLabel8.setBackground(new java.awt.Color(153, 153, 255));
        jLabel8.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("Nombre de Platillo:");
        jLayeredPane1.add(jLabel8);
        jLabel8.setBounds(40, 20, 181, 25);

        jLabel4.setBackground(new java.awt.Color(153, 153, 255));
        jLabel4.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Precio:");
        jLabel4.setName(""); // NOI18N
        jLayeredPane1.add(jLabel4);
        jLabel4.setBounds(40, 100, 65, 25);

        btn_Listar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_2.png"))); // NOI18N
        btn_Listar.setText("jLabel3");
        btn_Listar.setToolTipText("Listar");
        btn_Listar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_ListarMouseMoved(evt);
            }
        });
        btn_Listar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_ListarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_ListarMouseExited(evt);
            }
        });
        jLayeredPane1.add(btn_Listar);
        btn_Listar.setBounds(330, 70, 100, 35);

        btn_Ordenar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_1.png"))); // NOI18N
        btn_Ordenar.setText("jLabel3");
        btn_Ordenar.setToolTipText("Ordenar");
        btn_Ordenar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_OrdenarMouseMoved(evt);
            }
        });
        btn_Ordenar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_OrdenarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_OrdenarMouseExited(evt);
            }
        });
        jLayeredPane1.add(btn_Ordenar);
        btn_Ordenar.setBounds(470, 70, 100, 36);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo-01.jpg"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(610, 305));
        jLabel1.setName("PlaneUI"); // NOI18N
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel1MouseDragged(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });
        jLayeredPane1.add(jLabel1);
        jLabel1.setBounds(0, 0, 600, 300);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo-02-01-01.jpg"))); // NOI18N
        jLabel2.setToolTipText("");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setMaximumSize(new java.awt.Dimension(610, 305));
        jLabel2.setName(""); // NOI18N
        jLabel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel2MouseDragged(evt);
            }
        });
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel2MousePressed(evt);
            }
        });
        jLayeredPane1.add(jLabel2);
        jLabel2.setBounds(0, 0, 600, 300);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private int x;
    private int y;
    private void btn_OrdenarMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_OrdenarMouseMoved
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_1_hover.png");
        btn_Ordenar.setIcon(imagen);
    }//GEN-LAST:event_btn_OrdenarMouseMoved

    private void btn_OrdenarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_OrdenarMouseExited
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_1.png");
        btn_Ordenar.setIcon(imagen);        // TODO add your handling code here:
    }//GEN-LAST:event_btn_OrdenarMouseExited

    private void txt_NombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_NombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_NombreActionPerformed

    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jLabel1MousePressed

    private void jLabel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - x, point.y - y);
    }//GEN-LAST:event_jLabel1MouseDragged

    private void jLabel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jLabel2MousePressed

    private void jLabel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - x, point.y - y);
    }//GEN-LAST:event_jLabel2MouseDragged

    private void btn_ListarMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ListarMouseMoved
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_2_hover.png");
        btn_Listar.setIcon(imagen);
    }//GEN-LAST:event_btn_ListarMouseMoved

    private void btn_ListarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ListarMouseExited
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_2.png");
        btn_Listar.setIcon(imagen);
    }//GEN-LAST:event_btn_ListarMouseExited

    private void btn_cerrarMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarMouseMoved
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_3_hover-01.png");
        btn_cerrar.setIcon(imagen);
    }//GEN-LAST:event_btn_cerrarMouseMoved

    private void btn_cerrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarMouseExited
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_3-01.png");
        btn_cerrar.setIcon(imagen);
    }//GEN-LAST:event_btn_cerrarMouseExited

    private void btn_cerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarMouseClicked
        System.exit(0);
    }//GEN-LAST:event_btn_cerrarMouseClicked

    private void btn_ListarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ListarMouseClicked
        jLabel1.setVisible(false);
        jLabel2.setVisible(true);
        Lista_Plat.Listar(modTabla);
    }//GEN-LAST:event_btn_ListarMouseClicked

    private void btn_OrdenarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_OrdenarMouseClicked
        jLabel2.setVisible(false);
        jLabel1.setVisible(true);
        Lista_Plat.Ordenar(Lista_Plat, 0, Lista_Plat.getI()-1);
        Lista_Plat.Listar(modTabla);
    }//GEN-LAST:event_btn_OrdenarMouseClicked

    private void txt_CodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_CodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_CodigoActionPerformed

    private void txt_PrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_PrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_PrecioActionPerformed

    private void btn_RegistrarMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_RegistrarMouseMoved
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_4_hover.png");
        btn_Registrar.setIcon(imagen);
    }//GEN-LAST:event_btn_RegistrarMouseMoved

    private void btn_RegistrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_RegistrarMouseClicked
        
        if(txt_Nombre.getText().isEmpty()||txt_Precio.getText().isEmpty()||txt_Codigo.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(this, "Llene todos los campos...");
        }
        else
        {String Nombre = txt_Nombre.getText();
        double Precio = Double.parseDouble(txt_Precio.getText());
        int Codigo = Integer.parseInt(txt_Codigo.getText());
            Platillos Elemento = new Platillos(Nombre, Precio, Codigo);
            Lista_Plat.Agregar_Platillos(Elemento);
            JOptionPane.showMessageDialog(this, "Registro exitoso...");
        }
    }//GEN-LAST:event_btn_RegistrarMouseClicked

    private void btn_RegistrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_RegistrarMouseExited
        ImageIcon imagen = new ImageIcon("src/imagenes/boton_4.png");
        btn_Registrar.setIcon(imagen);
    }//GEN-LAST:event_btn_RegistrarMouseExited
    public void keyTyped(KeyEvent e) {
        char caracter = e.getKeyChar();
        // Verificar si la tecla pulsada no es un digito
        if (Character.isLetter(caracter)||Character.isIdeographic(FRAMEBITS)) //              && (caracter != '\b' /*corresponde atecl de retroceso, porsia caso*/))
        {
            e.consume();  // ignorar el evento de teclado
        }
    }

    public void keyTyped_letras(KeyEvent ke) 
    {
        char c = ke.getKeyChar();
        if (Character.isDigit(c)) {
            getToolkit().beep();//sonido de bip 
            ke.consume();
            JOptionPane.showMessageDialog(this, "Ingresa Solo Letras");
        }
    }
    private void txt_PrecioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_PrecioKeyPressed
        
    }//GEN-LAST:event_txt_PrecioKeyPressed

    private void txt_PrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_PrecioKeyTyped
        keyTyped(evt);
    }//GEN-LAST:event_txt_PrecioKeyTyped

    private void txt_NombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_NombreKeyTyped
        keyTyped_letras(evt);
    }//GEN-LAST:event_txt_NombreKeyTyped

    private void txt_CodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_CodigoKeyTyped
        keyTyped(evt);
    }//GEN-LAST:event_txt_CodigoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrame_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrame_Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_Listar;
    private javax.swing.JLabel btn_Ordenar;
    private javax.swing.JLabel btn_Registrar;
    private javax.swing.JLabel btn_cerrar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_Datos;
    private javax.swing.JTextField txt_Codigo;
    private javax.swing.JTextField txt_Nombre;
    private javax.swing.JTextField txt_Precio;
    // End of variables declaration//GEN-END:variables
}
